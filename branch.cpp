#include <string>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <fstream>
#include <math.h>
#include <cmath>

using namespace std;

string convert_binary(string direction, int bht){

    /* Esta función se encarga de convertir un número de decimal a binario. Recibe el número a convertir (direction)
    Y recibe la cantidad de bits que se deben convertir (bht). Devuelve un string que representa los
    números en binario convertidos.
    */

    string result = "";
    long long number = stol(direction); //Se convierte la dirección a un número
    int reminder = 0;
    
    for(int i = 0; i < bht; i++){ //Se utiliza el algoritmo de sacar el módulo y dividir entre dos para obtener cada dígito binario
        reminder = number%2;
        result=to_string(reminder)+result;
        number=number/2;
    }
    return result;
}

int convert_decimal(string direction){

    /* Esta función convierte un número en binario a decimal. Recibe el string con los dígitos
    en binario (direction) y devuelve un entero con el número en decimal (result)
    */

    int result = 0;
    int power = 2;

    if(direction[direction.length()-1]=='0'){ //Se obtiene el dígito menos significativo del número binario
        result = 0;
    } else {
        result = 1;
    }

    int position = 0, integ = 0;
    char charac;

    for(int i = 1; i<direction.length(); i++){ //Se obtiene cada bit siguiente, se multiplica por la potencia de 2 correspondiente
        // a la posición y se suma al resultado para obtener el valor en decimal

        position = direction.length()-i-1;
        charac = direction[position];
        integ = charac - '0';
        result += power*integ;
        power *= 2;

    }
    return result;
}

void print_table(string type, int bht, int hist_p, int hist_g, int correct_t, int incorrect_t, int branch_num, int correct_n, int incorrect_n){

    /* Esta función se encarga de imprimir en la terminal la tabla con los resultados finales. Recibe el tipo de predictor (type),
    El tamaño de la tabla bht, el tamaño de los registros de historia privada (hist_p), el tamaño del registro de historia global (hist_g),
    la cantidad de saltos correctos tomados (correct_t) y no tomados (correct_n), la cantidad de saltos incorrectos tomados (incorrect_t)
    y no tomados (incorrect_n), y la cantidad total de saltos (branch_num). No tiene valor de retorno.
    */

    double percentage = ((correct_t + correct_n)*100.0) / (branch_num); //Se obtiene el porcentage de saltos correctos

    cout << "_________________________________________________________" << endl;
    cout << "Prediction parameters:" << endl;
    cout << "_________________________________________________________" << endl;
    cout << "Branch prediction type:                          " << type << endl;
    cout << "BHT size (entries):                              " << bht << endl;
    cout << "Global history register size:                    " << hist_g << endl;
    cout << "Private history register size:                   " << hist_p <<endl;
    cout << "_________________________________________________________" << endl;
    cout << "Simulation results:                              " << endl;
    cout << "_________________________________________________________" << endl;
    cout << "Number of branch:                                " << branch_num << endl;
    cout << "Number of correct prediction of taken branches:  " << correct_t << endl;
    cout << "Number of incorrect prediction of taken branches:" << incorrect_t << endl;
    cout << "Correct prediction of not taken branches:        " << correct_n << endl;
    cout << "Incorrect prediction of not taken branches:      " << incorrect_n << endl;
    cout << "Percentage of correct predictions:               " << percentage << endl;
    cout << "_________________________________________________________" << endl;



}

void bimodal(int bht, int hist_p, int hist_g, int revision){

    /*Esta función corresponde al predictor bimodal. Recibe como entradas el tamaño de la tabla BHT, el tamaño del registro de historia global
    (hist_g), que se utiliza solo para el llamado a la función de impresión, el tamaño de los registros de la tabla PHT (hist_p), de igual forma
    solo para impresión, y un parámetro para indicar si se desea o no crear el archivo con los primeros 5000 resultados (revision).
    No tiene valor de retorno
    */

    int correct_t=0, incorrect_t=0, branch_num=0; //Se definen las variables que van a guardar los datos que se deben mostrar en la tabla
    int correct_n=0, incorrect_n=0;

    int counter_rev = 0; //Contador utilizado para la parte de impresión de los primeros 5000 resultados

    string read = "";
    string prediction = "";
    string state = "";
    string branch = "";

    int power = 1;
    for(int a = 0; a < bht; a++){ //Se obtiene el tamaño de la tabla BHT (2^s)
        power*=2;
    }
    
    string table[power]; //Se define el arreglo que tendrá los contadores de 2 bits
    for(int b = 0; b < power; b++){ //Se inicializan en el estado 00, o strongly not taken
        table[b] = "00";
    }
    ofstream archivo;
    string to_file=""; //Esta variable llevará lo que se debe escribir en el archivo
    if(revision==1){ //Si se desea hacer la revisión, se abre el archivo donde se quiere escribir

        archivo.open ("bimodal.txt");
        archivo << "PC     Outcome     Prediction     correct/incorrect\n"; //Se escribe la primera línea con los títulos

    }
        string convert = "";
        long direction = 0;

        while(cin>>read){ //Se empieza con la lectura de cada PC, y se sigue el ciclo mientras no se llegue al final del archivo

            to_file=read; //Se pone el PC en lo que se escribirá al archivo
            branch_num++;
            convert = convert_binary(read,bht); //Se convierte a binario los últimos s bits del PC
            direction = convert_decimal(convert); //Se convierten a decimal el valor en binario
            state = table[direction]; //Se obtiene el estado actual del contador necesario para hacer la predicción

            if(state=="00"){ //Se hace la predicción en base al estado actual
                prediction = "N";
            } else if(state=="01"){
                prediction = "N";
            } else if(state=="10"){
                prediction = "T";
            } else {
                prediction = "T";
            }

            cin >> branch; //Se obtiene el outcome del salto
            to_file+="   "; //Se guarda el outcome y la predicción en lo que se escribirá en el archivo
            to_file+= branch;
            to_file+="   ";
            to_file+= prediction;

            if(prediction==branch && prediction == "T"){ //Se verifica si se realizó la predicción de forma correcta 
                //y se actualiza el arreglo de contadores. También se suma 1 a los saltos correctos o incorrectos, según sea el caso.

                to_file+="     correct";
                correct_t++;
                if(state=="10"){
                    table[direction] = "11";
                }

            } else if(prediction==branch && prediction == "N"){
                to_file+="     correct";
                if(state=="01"){
                    table[direction] = "00";
                }

                correct_n++;

            } else if(prediction!=branch && prediction == "T"){
                to_file+="     incorrect";
                if(state=="11"){
                    table[direction] = "10";
                } else {
                    table[direction] = "01";
                }

                incorrect_t++;

            } else if(prediction!=branch && prediction == "N"){
                to_file+="     incorrect";
                if(state=="00"){
                    table[direction] = "01";
                } else {
                    table[direction] = "10";
                }

                incorrect_n++;

            }
        
        if(counter_rev<5000 && revision==1){ //Si se seleccionó la revisión, se escribe el resultado al archivo, para los primeros 5000
            archivo << to_file;
            archivo << "\n";
        }
        counter_rev++; // Se suma 1 al contador de revisión
        
        }

        if(revision==1){ //Se cierra el archivo cuando se termina de escribir
            archivo.close();   
        }

        print_table("Bimodal",bht,hist_p,hist_g,correct_t,incorrect_t,branch_num,correct_n,incorrect_n); //Se llama a la función para 
        //imprimir los resultados
   
}

void hist_global(int bht, int hist_p, int hist_g, int revision){

    /*Esta función corresponde al predictor de historia global. Recibe como entradas el tamaño de la tabla BHT, el tamaño del registro 
    de historia global (hist_g), el tamaño de los registros de la tabla PHT (hist_p), que se utiliza solo para impresión, 
    y un parámetro para indicar si se desea o no crear el archivo con los primeros 5000 resultados (revision).
    No tiene valor de retorno
    */

    int correct_t=0, incorrect_t=0, branch_num=0; //Se definen las variables que van a guardar los datos que se deben mostrar en la tabla
    int correct_n=0, incorrect_n=0;

    int regist[hist_g] = {0}; //Se crea el registro de historia global y se inicializa en 0

    int counter_rev = 0; //Contador utilizado para la parte de impresión de los primeros 5000 resultados

    string read = "";
    string prediction = "";
    string state = "";
    string branch = "";
    string reg_string = "";

    int power = 1;
    for(int a = 0; a < bht; a++){ //Se obtiene el tamaño de la tabla BHT (2^s)
        power*=2;
    }
    
    string table[power]; //Se define el arreglo que tendrá los contadores de 2 bits
    for(int b = 0; b < power; b++){ //Se inicializan en el estado 00, o strongly not taken
        table[b] = "00";
    }
    ofstream archivo;
    string to_file=""; //Esta variable llevará lo que se debe escribir en el archivo
    if(revision==1){ //Si se desea hacer la revisión, se abre el archivo donde se quiere escribir

        archivo.open ("gshare.txt");
        archivo << "PC     Outcome     Prediction     correct/incorrect\n"; //Se escribe la línea con los títulos

    }
        string convert = "";
        long direction = 0;
        string result_xor = "";

        while(cin>>read){ //Se empieza con la lectura de cada PC, y se sigue mientras no se llegue al final del archivo

            to_file=read;
            branch_num++;
            reg_string = "";
            convert = convert_binary(read,bht); //Se convierte a binario los últimos s bits del PC
    
            for(int i = 0; i< hist_g; i++){ //Se convierte el registro de historia a string
                reg_string+=to_string(regist[i]);
            }

            if(convert.length()>reg_string.length()){ //Si s es mayor que la longitud del registro de historia
                //Se le ponen ceros a la izquierda al registro para que tenga la misma cantidad de bits que el PC

                for(int i = hist_g; i < convert.length(); i++){ 
                    reg_string = "0" + reg_string;
                }
            } else if(convert.length()<reg_string.length()){ //En caso contrario, se toman los últimos s bits del registro de historia
                reg_string=reg_string.substr(reg_string.length()-convert.length(),reg_string.length());
            }

            result_xor = "";
            
            for(int i = 0; i < convert.length(); i++){ //Se realiza el xor entre el PC y el registro de historia global

                if(reg_string[i]=='1' && reg_string[i]==convert[i]){
                    result_xor+="0";
                } else if(reg_string[i]!=convert[i]){
                    result_xor+="1";
                } else {
                    result_xor+="0";
                }
                
            }

            direction = convert_decimal(result_xor); //Se convierten a decimal los últimos "s" bits del resultado del xor
            state = table[direction]; //Se obtiene el estado actual del contador necesario para hacer la predicción

            if(state=="00"){ //Se hace la predicción en base al estado actual
                prediction = "N";
            } else if(state=="01"){
                prediction = "N";
            } else if(state=="10"){
                prediction = "T";
            } else {
                prediction = "T";
            }

            cin >> branch; //Se obtiene el outcome del salto y se agrega, junto con la predicción, a lo que se escribirá en el archivo
            to_file+="   ";
            to_file+= branch;
            to_file+="   ";
            to_file+= prediction;

            for(int i = 0; i < hist_g-1; i++){ //Se hace shift a la izquierda a los valores en el registro de historia global

                regist[i] = regist[i+1];
            }
            if(branch=="T"){ //Se actualiza el valor del último bit dependiendo de si se debía hacer el salto o no
                regist[hist_g-1] = 1;
            } else {
                regist[hist_g-1] = 0;
            }

            if(prediction==branch && prediction == "T"){ //Se verifica si se realizó la predicción de forma correcta y 
                //se actualiza el arreglo de contadores. También se suma 1 al contador correspondiente si se hace o no un salto
                //de forma correcta o incorrecta

                to_file+="     correct";
                correct_t++;
                if(state=="10"){
                    table[direction] = "11";
                }

            } else if(prediction==branch && prediction == "N"){
                to_file+="     correct";
                if(state=="01"){
                    table[direction] = "00";
                }

                correct_n++;

            } else if(prediction!=branch && prediction == "T"){
                to_file+="     incorrect";
                if(state=="11"){
                    table[direction] = "10";
                } else {
                    table[direction] = "01";
                }

                incorrect_t++;

            } else if(prediction!=branch && prediction == "N"){
                to_file+="     incorrect";
                if(state=="00"){
                    table[direction] = "01";
                } else {
                    table[direction] = "10";
                }

                incorrect_n++;

            }

        if(counter_rev<5000 && revision==1){ //Si se seleccionó la revisión, se escribe el resultado al archivo, para los primeros 5000
            archivo << to_file;
            archivo << "\n";
        }
        counter_rev++; //Se suma 1 al contador de revisión
        
        }

        if(revision==1){ //Se cierra el archivo cuando se termina de escribir
            archivo.close();   
        }

        print_table("gshare",bht,hist_p,hist_g,correct_t,incorrect_t,branch_num,correct_n,incorrect_n); 
        //Se llama a la función para imprimir los resultados

}

void hist_privada(int bht, int hist_p, int hist_g, int revision){

    /*Esta función corresponde al predictor de historia privada. Recibe como entradas el tamaño de la tabla BHT, el tamaño del registro 
    de historia global (hist_g), solo para cuando se llama la función de impresión, el tamaño de los registros de la tabla PHT (hist_p), 
    y un parámetro para indicar si se desea o no crear el archivo con los primeros 5000 resultados (revision).
    No tiene valor de retorno
    */

    int correct_t=0, incorrect_t=0, branch_num=0; //Se definen las variables que van a guardar los datos que se deben mostrar en la tabla
    int correct_n=0, incorrect_n=0;

    int counter_rev = 0; //Contador utilizado para la parte de impresión de los primeros 5000 resultados

    string read = "";
    string prediction = "";
    string state = "";
    string branch = "";
    string reg_string = "";

    int power = 1;
    for(int a = 0; a < bht; a++){ //Se obtiene el tamaño de la tabla BHT (2^s)
        power*=2;
    }

    int pht[power][hist_p]; //Se define la tabla PHT con los registros de historia privada

    for(int i = 0; i < power; i++){ //Se inicializan todos los valores de la PHT en 0
        for(int a = 0; a < hist_p; a++){
            pht[i][a]=0;
        }
    }

    string table[power]; //Se define el arreglo que tendrá los contadores de 2 bits
    
    for(int b = 0; b < power; b++){ //Se inicializan en el estado 00, o strongly not taken
        table[b] = "00";
    }
    ofstream archivo;
    string to_file=""; //Esta variable llevará lo que se debe escribir en el archivo
    if(revision==1){ //Si se desea hacer la revisión, se abre el archivo donde se quiere escribir

        archivo.open ("pshare.txt");
        archivo << "PC     Outcome     Prediction     correct/incorrect\n"; //Se imprime la línea con los títulos

    }
        string convert = "";
        long direction = 0;
        long direction2 = 0;
        string result_xor = "";

        while(cin>>read){ //Se empieza con la lectura de cada PC, y se sigue mientras no se llegue al final del archivo

            to_file=read;
            branch_num++;
            reg_string = "";
            convert = convert_binary(read,bht); //Se convierte a binario los últimos "s" bits del PC
            direction = convert_decimal(convert); //Se convierten a decimal el número en binario

            for(int i = 0; i< hist_p; i++){ //Se convierte la entrada de la pht a string
                reg_string+=to_string(pht[direction][i]);
            }

            if(convert.length()>reg_string.length()){//Si s es mayor a la cantidad de bits de una entrada de la pht
                //Se le ponen ceros a la izquierda para que tenga "s" bits

                for(int i = hist_p; i < convert.length(); i++){ 
                    reg_string = "0" + reg_string;
                }
                
            } else if(convert.length()<reg_string.length()){//En el caso contrario, se toman los s bits menos significativos del registro
            //De historia global

                reg_string=reg_string.substr(reg_string.length()-convert.length(),reg_string.length());
            }

            result_xor = "";
            
            for(int i = 0; i < convert.length(); i++){ //Se hace la operación xor entre el registro de historia y los últimos s bits del PC

                if(reg_string[i]=='1' && reg_string[i]==convert[i]){
                    result_xor+="0";
                } else if(reg_string[i]!=convert[i]){
                    result_xor+="1";
                } else {
                    result_xor+="0";
                }
                
            }

            direction2 = convert_decimal(result_xor); //Se convierten a decimal los últimos "s" números en binario del resultado del xor
            state = table[direction2]; //Se obtiene el estado actual del contador necesario para hacer la predicción
            
            if(state=="00"){ //Se hace la predicción en base al estado actual
                prediction = "N";
            } else if(state=="01"){
                prediction = "N";
            } else if(state=="10"){
                prediction = "T";
            } else {
                prediction = "T";
            }

            cin >> branch; //Se obtiene el outcome del salto
            to_file+="   "; //Se agrega el outcome y la predicción a lo que se escribe en el archivo
            to_file+= branch;
            to_file+="   ";
            to_file+= prediction;

            for(int i = 0; i < hist_p-1; i++){ //Se hace shift a la izquierda a los valores en el registro de historia global

                pht[direction][i] = pht[direction][i+1];
            }
            if(branch=="T"){ //Se actualiza el valor del último bit dependiendo de si se debía hacer el salto o no
                pht[direction][hist_p-1] = 1;
            } else {
                pht[direction][hist_p-1] = 0;
            }

            if(prediction==branch && prediction == "T"){ //Se verifica si se realizó la predicción de forma correcta y se actualiza
            // el arreglo de contadores, además de modificar el contador necesario si se realizó o no el salto de forma correcta o incorrecta

                to_file+="     correct";
                correct_t++;
                if(state=="10"){
                    table[direction2] = "11";
                }

            } else if(prediction==branch && prediction == "N"){
                to_file+="     correct";
                if(state=="01"){
                    table[direction2] = "00";
                }

                correct_n++;

            } else if(prediction!=branch && prediction == "T"){
                to_file+="     incorrect";
                if(state=="11"){
                    table[direction2] = "10";
                } else {
                    table[direction2] = "01";
                }

                incorrect_t++;

            } else if(prediction!=branch && prediction == "N"){
                to_file+="     incorrect";
                if(state=="00"){
                    table[direction2] = "01";
                } else {
                    table[direction2] = "10";
                }

                incorrect_n++;

            }
            
        if(counter_rev<5000 && revision==1){ //Si se seleccionó la revisión, se escribe el resultado al archivo, para los primeros 5000
            archivo << to_file;
            archivo << "\n";
        }
        counter_rev++; //Se suma 1 al contador de revisión
        
        }

        if(revision==1){ //Se cierra el archivo cuando se termina de escribir
            archivo.close();   
        }

        print_table("pshare",bht,hist_p,hist_g,correct_t,incorrect_t,branch_num,correct_n,incorrect_n); //Se llama a la función 
        //para imprimir los resultados

}

void torneo(int bht, int hist_p, int hist_g, int revision){

    /*Esta función corresponde al predictor de torneo. Recibe como entradas el tamaño de la tabla BHT, el tamaño del registro 
    de historia global (hist_g), el tamaño de los registros de la tabla PHT (hist_p), y un parámetro para indicar si se desea 
    o no crear el archivo con los primeros 5000 resultados (revision).
    No tiene valor de retorno
    */

    int correct_t=0, incorrect_t=0, branch_num=0; //Se definen las variables que van a guardar los datos que se deben mostrar en la tabla
    int correct_n=0, incorrect_n=0;

    int regist[hist_g] = {0}; //Se inicializa el registro de historia global con todos sus valores iguales a 0

    int counter_rev = 0; //Contador utilizado para la parte de impresión de los primeros 5000 resultados

    int gshare = 0, pshare = 0; //Variables que se utilizarán para verificar cuál predictor predijo de forma correcta

    string read = "";
    string prediction_g = "";
    string state_g = "";
    string branch = "";
    string reg_string_g = "";

    string read_p = "";
    string prediction_p = "";
    string state_p = "";
    string reg_string_p = "";

    string state_m = "";

    int power = 1;
    for(int a = 0; a < bht; a++){ //Se obtiene el tamaño de la tabla BHT (2^s)
        power*=2;
    }

    int pht[power][hist_p]; //Se crea la PHT

    for(int i = 0; i < power; i++){ //Se inicializa todas las entradas de la PHT en 0
        for(int a = 0; a < hist_p; a++){
            pht[i][a]=0;
        }
    }
    
    string table_p[power]; //Se define el arreglo que tendrá los contadores de 2 bits de la BHT del predictor pshare
    for(int b = 0; b < power; b++){ //Se inicializan en el estado 00, o strongly not taken
        table_p[b] = "00";
    }
    
    string table_g[power]; //Se define el arreglo que tendrá los contadores de 2 bits de la BHT del predictor gshare
    for(int b = 0; b < power; b++){ //Se inicializan en el estado 00, o strongly not taken
        table_g[b] = "00";
    }

    string metapredictor[power]; //Se define el arreglo que tendrá los contadores de 2 bits para el metapredictor
    for(int b = 0; b < power; b++){ //Se inicializan en el estado 11, o strongly prefered pshare
        metapredictor[b] = "11";
    }


    ofstream archivo;
    string to_file=""; //Esta variable llevará lo que se debe escribir en el archivo
    if(revision==1){ //Si se desea hacer la revisión, se abre el archivo donde se quiere escribir

        archivo.open ("torneo.txt");
        archivo << "PC     Outcome  prediction  Predictor   correct/incorrect\n"; //Se escribe la línea con los títulos

    }
        string convert = "";
        long direction_g = 0;
        string result_xor_g = "";

        string convert_p = "";
        long direction_p = 0;
        long direction2 = 0;
        string result_xor_p = "";

        while(cin>>read){ //Se empieza con la lectura de cada PC, y se repite cada ciclo mientras no se llegue al final del archivo

            to_file=read;
            branch_num++;
            reg_string_g = "";
	        reg_string_p = "";
            convert = convert_binary(read,bht); //Se convierte a binario los últimos "s" bits del PC

            direction_p = convert_decimal(convert); //Se convierten a decimal los últimos "s" bits

            for(int i = 0; i< hist_p; i++){ //Se convierte la entrada de la PHT a string
                reg_string_p+=to_string(pht[direction_p][i]);
            }

            if(convert.length()>reg_string_p.length()){ //Si "s" es mayor al tamaño de los registros de historia privada,
                //Se le ponen ceros a la izquierda para que tenga "s" bits

                for(int i = hist_p; i < convert.length(); i++){ 
                    reg_string_p = "0" + reg_string_p;
                }
            } else if(convert.length()<reg_string_p.length()){ //En el caso contrario se toman los últimos "s" bits del registro de historia privada
                reg_string_p=reg_string_p.substr(reg_string_p.length()-convert.length(),reg_string_p.length());
            }

            for(int i = 0; i< hist_g; i++){ //Se convierte el registro de historia global a string
                reg_string_g+=to_string(regist[i]);
            }

            if(convert.length()>reg_string_g.length()){ //Si "s" es mayor al tamaño de los registros de historia global,
                //Se le ponen ceros a la izquierda para que tenga "s" bits
                for(int i = hist_g; i < convert.length(); i++){ 
                    reg_string_g = "0" + reg_string_g;
                }
            } else if(convert.length()<reg_string_g.length()){ //En el caso contrario se toman los últimos "s" bits del registro de historia global
                reg_string_g=reg_string_g.substr(reg_string_g.length()-convert.length(),reg_string_g.length());
            }

            result_xor_g = "";
            result_xor_p = "";

            for(int i = 0; i < convert.length(); i++){ //Se hace el xor del registro de historia privada con los últimos "s" bits del PC

                if(reg_string_p[i]=='1' && reg_string_p[i]==convert[i]){
                    result_xor_p+="0";
                } else if(reg_string_p[i]!=convert[i]){
                    result_xor_p+="1";
                } else {
                    result_xor_p+="0";
                }
                
            }
            
            for(int i = 0; i < convert.length(); i++){ //Se hace el xor del registro de historia global con los últimos "s" bits del PC

                if(reg_string_g[i]=='1' && reg_string_g[i]==convert[i]){
                    result_xor_g+="0";
                } else if(reg_string_g[i]!=convert[i]){
                    result_xor_g+="1";
                } else {
                    result_xor_g+="0";
                }
                
            }
            
            direction2 = convert_decimal(result_xor_p); //Se convierten a decimal los últimos "s" números en binario del resultado xor pshare
            state_p = table_p[direction2]; //Se obtiene el estado actual del contador necesario para hacer la predicción, del predictor pshare


            direction_g = convert_decimal(result_xor_g); //Se convierten a decimal los últimos "s" números en binario del resultado xor gshare
            state_g = table_g[direction_g]; //Se obtiene el estado actual del contador necesario para hacer la predicción, del predictor gshare

            if(state_p=="00"){ //Se hace la predicción en base al estado actual, para el predictor pshare
                prediction_p = "N";
            } else if(state_p=="01"){
                prediction_p = "N";
            } else if(state_p=="10"){
                prediction_p = "T";
            } else {
                prediction_p = "T";
            }

            if(state_g=="00"){ //Se hace la predicción en base al estado actual, para el predictor gshare
                prediction_g = "N";
            } else if(state_g=="01"){
                prediction_g = "N";
            } else if(state_g=="10"){
                prediction_g = "T";
            } else {
                prediction_g = "T";
            }

            cin >> branch; //Se obtiene el outcome del salto y se agrega a lo que se va a escribir en el archivo
            to_file+="   ";
            to_file+= branch;
            to_file+="   ";

            for(int i = 0; i < hist_p-1; i++){ //Se hace shift a la izquierda a los valores en el registro de historia privada

                pht[direction_p][i] = pht[direction_p][i+1];
            }
            if(branch=="T"){ //Se actualiza el valor del último bit dependiendo de si se debía hacer el salto o no, 
            //para el registro de historia privada
                pht[direction_p][hist_p-1] = 1;
            } else {
                pht[direction_p][hist_p-1] = 0;
            }


            for(int i = 0; i < hist_g-1; i++){ //Se hace shift a la izquierda a los valores en el registro de historia global

                regist[i] = regist[i+1];
            }
            if(branch=="T"){ //Se actualiza el valor del último bit dependiendo de si se debía hacer el salto o no,
            //para el registro de historia global
                regist[hist_g-1] = 1;
            } else {
                regist[hist_g-1] = 0;
            }

            if(prediction_p==branch && prediction_p == "T"){ //Se verifica si se realizó la predicción de forma correcta 
            //y se actualiza el arreglo de contadores, para el predictor pshare. La variable pshare se pone en 1 si la predicción
            //fue correcta, y se pone en 0 si fue incorrecta.

                if(state_p=="10"){
                    table_p[direction2] = "11";
                }

                pshare=1;

            } else if(prediction_p==branch && prediction_p == "N"){

                if(state_p=="01"){
                    table_p[direction2] = "00";
                }

                pshare=1;

            } else if(prediction_p!=branch && prediction_p == "T"){

                if(state_p=="11"){
                    table_p[direction2] = "10";
                } else {
                    table_p[direction2] = "01";
                }

                pshare=0;

            } else if(prediction_p!=branch && prediction_p == "N"){

                if(state_p=="00"){
                    table_p[direction2] = "01";
                } else {
                    table_p[direction2] = "10";
                }

                pshare=0;

            }

            if(prediction_g==branch && prediction_g == "T"){ //Se verifica si se realizó la predicción de forma correcta
            // y se actualiza el arreglo de contadores, para el predictor gshare. La variable gshare se pone en 1 si la predicción
            //fue correcta, y se pone en 0 si fue incorrecta.

                if(state_g=="10"){
                    table_g[direction_g] = "11";
                }

                gshare=1;

            } else if(prediction_g==branch && prediction_g == "N"){
                
                if(state_g=="01"){
                    table_g[direction_g] = "00";
                }

                gshare=1;

            } else if(prediction_g!=branch && prediction_g == "T"){
                
                if(state_g=="11"){
                    table_g[direction_g] = "10";
                } else {
                    table_g[direction_g] = "01";
                }

                gshare=0;

            } else if(prediction_g!=branch && prediction_g == "N"){

                if(state_g=="00"){
                    table_g[direction_g] = "01";
                } else {
                    table_g[direction_g] = "10";
                }

                gshare=0;

            }

            state_m = metapredictor[direction_p]; //Se obtiene el estado del metapredictor, dado por los últimos "s" bits del PC

            if(pshare==1 && gshare==1){ //Si ambos predictores aciertan, no se actualiza el estado del metapredictor

                if(prediction_g=="N"){ //Se incrementa el valor de la variable correspondiente si el salto se hizo o no de forma correcta
                    correct_n++;
                } else {
                    correct_t++;
                }

                if(state_m=="00" || state_m=="01"){ //Dependiendo del estado se agrega al string que se escribirá en el archivo
                    //la predicción, el tipo de predictor utilizado y que el resultado fue correcto
                    to_file+=prediction_g;
                    to_file+="    G     correct";
                } else {
                    to_file+=prediction_p;
                    to_file+="    P     correct";
                }

            } else if(pshare==0 && gshare==1){ //Si pshare es 0 y gshare es 1 se actualiza el estado del metapredictor, dependiendo del 
                //estado actual, y se actualiza el string que se escribirá al archivo con la predicción, además del contador respectivo
                //a si se realizó el salto o no, de forma correcta o incorrecta.

                if(state_m=="01"){
                    metapredictor[direction_p]="00";
                    to_file+=prediction_g;
                    to_file+="    G     correct";

                    if(prediction_g=="N"){
                        correct_n++;
                    } else {
                        correct_t++;
                }

                } else if(state_m=="10"){
                    metapredictor[direction_p]="01";
                    to_file+=prediction_p;
                    to_file+="    P     incorrect";

                    if(prediction_p=="N"){
                        incorrect_n++;
                    } else {
                        incorrect_t++;
                }

                } else if(state_m=="11"){
                    metapredictor[direction_p]="10";
                    to_file+=prediction_p;
                    to_file+="    P     incorrect";

                    if(prediction_p=="N"){
                        incorrect_n++;
                    } else {
                        incorrect_t++;
                }

                } else{
                    to_file+=prediction_g;
                    to_file+="    G     correct";

                    if(prediction_g=="N"){
                        correct_n++;
                    } else {
                        correct_t++;
                }
                }

            } else if(pshare==1 && gshare==0){ //Si pshare es 1 y gshare es 0 se actualiza el estado del metapredictor, dependiendo del 
                //estado actual, y se actualiza el string que se escribirá al archivo con la predicción, además del contador respectivo
                //a si se realizó el salto o no, de forma correcta o incorrecta.

                if(state_m=="01"){
                    metapredictor[direction_p]="10";
                    to_file+=prediction_g;
                    to_file+="    G     incorrect";

                    if(prediction_g=="N"){
                        incorrect_n++;
                    } else {
                        incorrect_t++;
                }

                } else if(state_m=="10"){
                    metapredictor[direction_p]="11";
                    to_file+=prediction_p;
                    to_file+="    P     correct";

                    if(prediction_p=="N"){
                        correct_n++;
                    } else {
                        correct_t++;
                }

                } else if(state_m=="00"){
                    metapredictor[direction_p]="01";
                    to_file+=prediction_g;
                    to_file+="    G     incorrect";

                    if(prediction_g=="N"){
                        incorrect_n++;
                    } else {
                        incorrect_t++;
                }

                } else{
                    to_file+=prediction_p;
                    to_file+="    P     correct";

                    if(prediction_p=="N"){
                        correct_n++;
                    } else {
                        correct_t++;
                }
                }

            } else { //En este caso ambos predictores fallaron, por lo que no se actualiza el estado del metapredictor. 
                //Se actualiza el string que se escribirá al archivo con la predicción, además del contador respectivo 
                //a si se realizó el salto o no, de forma correcta o incorrecta.

                if(state_m=="00" || state_m=="01"){
                    to_file+=prediction_g;
                    to_file+="    G     incorrect";

                    if(prediction_g=="N"){
                        incorrect_n++;
                    } else {
                        incorrect_t++;
                }

                } else {
                    to_file+=prediction_p;
                    to_file+="    P     incorrect";

                    if(prediction_g=="N"){
                        incorrect_n++;
                    } else {
                        incorrect_t++;
                }

                }
                
            }     

        if(counter_rev<5000 && revision==1){ //Si se seleccionó la revisión, se escribe el resultado al archivo, para los primeros 5000
            archivo << to_file;
            archivo << "\n";
        }
        counter_rev++; //Se aumenta el contador de revisión
        
        }

        if(revision==1){ //Se cierra el archivo cuando se termina de escribir
            archivo.close();   
        }

        print_table("torneo",bht,hist_p,hist_g,correct_t,incorrect_t,branch_num,correct_n,incorrect_n); //Se llama a la función para 
        //imprimir los resultados
           
}

int main(int argc, char** argv){

    /*Función principal del programa. Esta recibe los argumentos necesarios para el funcionamiento del programa, y llama a la función
    correspondiente al predictor seleccionado por el usuario.
    */

    int bht = stoi(argv[2]); //Se utiliza para definir el tamaño de la tabla BHT
    int select = stoi(argv[4]); //Se utiliza para seleccionar el tipo de predictor a utilizar
    int hist_p = stoi(argv[8]); //Se utiliza para definir el tamaño de los registros de historia privada
    int hist_g = stoi(argv[6]); //Se utiliza para definir el tamaño del registro de historia global
    int revision = stoi(argv[10]); //En caso de ser 1, se utiliza para generar un archivo con los primeros 5000 resultados

    switch (select){ //Dependiendo del predictor seleccionado se llama a la función correspondiente

        case 0: bimodal(bht,hist_p,hist_g,revision);
        break;
        case 1: hist_privada(bht,hist_p,hist_g,revision);
        break;
        case 2: hist_global(bht,hist_p,hist_g,revision);
        break;
        case 3: torneo(bht,hist_p,hist_g,revision);
        break;

    }
    return 0;

}
