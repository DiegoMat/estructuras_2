Laboratorio 1, estructuras de computadores digitales II
==========

Este laboratorio consiste en crear 4 simuladores de predictores de salto. Se tiene un predictor bimodal, uno con historia global, uno con historia privada y un predictor con torneo.

El bimodal consiste de un arreglo de contadores de 2 bits que pueden tomar 4 estados, strongly not taken, weakly not taken, weakly taken y strongly taken. Dependiendo de los últimos
bits del PC se accede al contador correspondiente para realizar la predicción.

El de historia global es similar, pero para acceder a una posición del arreglo se usa el resultado de la operación xor entre los últimos bits del PC y un registro que lleva la historia
de los últimos saltos.

El de historia privada es similar, pero se tiene un arreglo de registros de historia privada en lugar de un solo registro, o la PHT, que es accedida con los últimos bits del PC.

Finalmente, en el de torneo se tienen el de historia global y el de historia privada funcionando al mismo tiempo, y se utiliza un metapredictor para seleccionar entre una predicción
u otra.

--------------------
Instrucciones de ejecución:
--------------------


Para ejecutar el programa, lo primero que se debe hacer es compilar el código:

g++ branch.cpp

Luego, se ejecuta de la siguiente forma:

gunzip -c branch-trace-gcc.trace.gz | ./a.out -s P1 -bp P2 -gh P3 -ph P4 -o P5

Donde: 

P1 representa el tamaño de la BHT.

P2 representa el tipo de predicción. 0 para bimodal, 1 para pshare, 2 para gshare y 3 para torneo.

P3 representa el tamaño del registro de historia global.

P4 representa el tamaño de los registros de historia privada.

P5 se pone en 1 si se desea imprimir un archivo para revisión, con los primeros 5000 resultados.


--------------------
Estudiante:
--------------------
+ Diego Matamoros Arnáez - B74566

